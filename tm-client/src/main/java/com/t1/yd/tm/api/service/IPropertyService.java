package com.t1.yd.tm.api.service;

import com.t1.yd.tm.api.endpoint.IConnectionProvider;
import org.jetbrains.annotations.NotNull;

public interface IPropertyService extends IConnectionProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorName();

    @NotNull
    String getAuthorEmail();

}
