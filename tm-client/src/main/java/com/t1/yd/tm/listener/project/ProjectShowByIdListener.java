package com.t1.yd.tm.listener.project;

import com.t1.yd.tm.api.endpoint.IProjectEndpoint;
import com.t1.yd.tm.api.service.ITokenService;
import com.t1.yd.tm.dto.model.ProjectDTO;
import com.t1.yd.tm.dto.request.project.ProjectShowByIdRequest;
import com.t1.yd.tm.dto.response.project.ProjectShowByIdResponse;
import com.t1.yd.tm.event.ConsoleEvent;
import com.t1.yd.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public final class ProjectShowByIdListener extends AbstractProjectListener {

    @NotNull
    public static final String NAME = "project_show_by_id";
    @NotNull
    public static final String DESCRIPTION = "Show project by Id";

    @Autowired
    public ProjectShowByIdListener(@NotNull final ITokenService tokenService,
                                   @NotNull final IProjectEndpoint projectEndpointClient) {
        super(tokenService, projectEndpointClient);
    }

    @Override
    @EventListener(condition = "@projectShowByIdListener.getName()==#consoleEvent.name")
    public void handle(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID:");

        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectShowByIdRequest request = new ProjectShowByIdRequest();
        request.setId(id);
        request.setToken(getToken());
        @NotNull final ProjectShowByIdResponse response = getProjectEndpointClient().showProjectById(request);
        @NotNull final ProjectDTO projectDTO = response.getProjectDTO();

        showProject(projectDTO);
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

}
