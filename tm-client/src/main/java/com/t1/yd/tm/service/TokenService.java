package com.t1.yd.tm.service;

import com.t1.yd.tm.api.service.ITokenService;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;

@Service
@Getter
@Setter
public class TokenService implements ITokenService {

    @Nullable
    private String token;

}
