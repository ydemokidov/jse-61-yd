package com.t1.yd.tm.model;

import com.t1.yd.tm.api.model.IWBS;
import com.t1.yd.tm.enumerated.Status;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Cacheable
@Table(name = "tasks")
@NoArgsConstructor
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class Task extends AbstractUserOwnedEntity implements IWBS {

    @NotNull
    @Column(length = 100, nullable = false)
    private String name = "";

    @NotNull
    @Column(length = 200, nullable = false)
    private String description = "";

    @NotNull
    @Column(length = 50, nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Nullable
    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project project;

    @NotNull
    @Column(nullable = false)
    private Date created = new Date();

    public Task(@NotNull User user, @NotNull String name, @NotNull String description) {
        super(user);
        this.name = name;
        this.description = description;
    }

    @NotNull
    @Override
    public String toString() {
        return name + " : " + description;
    }
}