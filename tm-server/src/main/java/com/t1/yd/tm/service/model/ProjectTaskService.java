package com.t1.yd.tm.service.model;

import com.t1.yd.tm.api.service.model.IProjectService;
import com.t1.yd.tm.api.service.model.IProjectTaskService;
import com.t1.yd.tm.api.service.model.ITaskService;
import com.t1.yd.tm.exception.entity.ProjectNotFoundException;
import com.t1.yd.tm.exception.entity.TaskNotFoundException;
import com.t1.yd.tm.exception.field.ProjectIdEmptyException;
import com.t1.yd.tm.exception.field.TaskIdEmptyException;
import com.t1.yd.tm.exception.field.UserIdEmptyException;
import com.t1.yd.tm.model.Project;
import com.t1.yd.tm.model.Task;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final ITaskService taskService;

    @Autowired
    public ProjectTaskService(@NotNull final IProjectService projectService,
                              @NotNull final ITaskService taskService) {
        this.projectService = projectService;
        this.taskService = taskService;
    }


    @Override
    @SneakyThrows
    @Transactional
    public void bindTaskToProject(@NotNull final String userId, @NotNull final String taskId, @NotNull final String projectId) {
        if (projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId.isEmpty()) throw new TaskIdEmptyException();
        if (userId.isEmpty()) throw new UserIdEmptyException();

        @Nullable final Project project = projectService.findOneById(userId, projectId);
        if (project == null)
            throw new ProjectNotFoundException();
        @Nullable final Task task = taskService.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProject(project);
        taskService.update(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeProjectById(@NotNull final String userId, @NotNull final String projectId) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (projectService.findOneById(userId, projectId) == null)
            throw new ProjectNotFoundException();
        @NotNull final List<Task> tasks = taskService.findAllByProjectId(userId, projectId);
        for (@NotNull final Task task : tasks) {
            taskService.removeById(task.getId());
        }
    }

    @Override
    @SneakyThrows
    @Transactional
    public void unbindTaskFromProject(@NotNull final String userId, @NotNull final String taskId, @NotNull final String projectId) {
        if (projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId.isEmpty()) throw new TaskIdEmptyException();
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (projectService.findOneById(userId, projectId) == null)
            throw new ProjectNotFoundException();
        @Nullable final Task task = taskService.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProject(null);
        taskService.update(task);
    }

}
