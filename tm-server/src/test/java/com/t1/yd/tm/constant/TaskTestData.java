package com.t1.yd.tm.constant;

import com.t1.yd.tm.dto.model.TaskDTO;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.List;

@UtilityClass
public class TaskTestData {

    @NotNull
    public static final TaskDTO USER_1_PROJECT_1_TASK_DTO_1 = new TaskDTO();

    @NotNull
    public static final TaskDTO USER_1_PROJECT_1_TASK_DTO_2 = new TaskDTO();

    @NotNull
    public static final TaskDTO ADMIN_PROJECT_1_TASK_DTO_1 = new TaskDTO();

    @NotNull
    public static final TaskDTO ADMIN_PROJECT_TASK_DTO_2 = new TaskDTO();

    @NotNull
    public final static String TASK_NAME = "task1 name";

    @NotNull
    public final static String TASK_DESCRIPTION = "task1 desc";

    @NotNull
    public static List<TaskDTO> ALL_TASK_DTOS = Arrays.asList(USER_1_PROJECT_1_TASK_DTO_1, USER_1_PROJECT_1_TASK_DTO_2, ADMIN_PROJECT_1_TASK_DTO_1, ADMIN_PROJECT_TASK_DTO_2);

    @NotNull
    public static List<TaskDTO> USER1_PROJECT1_TASK_DTOS = Arrays.asList(USER_1_PROJECT_1_TASK_DTO_1, USER_1_PROJECT_1_TASK_DTO_2);

}
